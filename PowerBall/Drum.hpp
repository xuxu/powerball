//
//  Drum.hpp
//  PowerBall
//
//  Created by Vivo Xu on 1/14/16.
//  Copyright © 2016 hiVivo. All rights reserved.
//

#ifndef Drum_hpp
#define Drum_hpp

#include <vector>
#include <mutex>
#include "Ball.hpp"

class Drum
{
    std::vector<Ball>   m_balls;
    std::mutex          m_balls_mutex;
    
public:
    Drum();
    
    void fill(Ball ball);
    
    void roll();
    
    Ball draw();
    
    std::vector<Ball> peek();
};

#endif /* Drum_hpp */

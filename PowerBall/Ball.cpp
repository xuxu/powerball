//
//  Ball.cpp
//  PowerBall
//
//  Created by Vivo Xu on 1/14/16.
//  Copyright © 2016 hiVivo. All rights reserved.
//

#include "Ball.hpp"

Ball::Ball(int number, Color color) : m_number(number), m_color(color)
{
}

std::string Ball::text()
{
    if (m_color == Color::White)
    {
        return std::to_string(m_number);
    }
    else
    {
        return "(" + std::to_string(m_number) + ")";
    }
}
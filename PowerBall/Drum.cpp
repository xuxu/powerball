//
//  Drum.cpp
//  PowerBall
//
//  Created by Vivo Xu on 1/14/16.
//  Copyright © 2016 hiVivo. All rights reserved.
//

#include "Drum.hpp"
#include <algorithm>
#include <chrono>
#include <random>
#include <cassert>
using namespace std;

Drum::Drum()
{
    
}

void Drum::fill(Ball ball)
{
    lock_guard<mutex> lock(m_balls_mutex);
    m_balls.push_back(ball);
}

void Drum::roll()
{
    // initial seed by time
    unsigned seed = (unsigned)chrono::system_clock::now().time_since_epoch().count();
    // initial generator by seed
    minstd_rand0 generator(seed);
    
    lock_guard<mutex> lock(m_balls_mutex);
    shuffle(m_balls.begin(), m_balls.end(), generator);
}

Ball Drum::draw()
{
    lock_guard<mutex> lock(m_balls_mutex);
    assert(!m_balls.empty());
    
    Ball result = m_balls.back();
    m_balls.pop_back();
    
    return result;
}

vector<Ball> Drum::peek()
{
    lock_guard<mutex> lock(m_balls_mutex);
    return m_balls;
}
//
//  main.cpp
//  PowerBall
//
//  Created by Vivo Xu on 1/14/16.
//  Copyright © 2016 hiVivo. All rights reserved.
//

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <vector>
#include <set>
#include <chrono>
#include <random>
#include <map>
#include <thread>
#include "Drum.hpp"
using namespace std;

const int WHITE_SIZE = 5;
const int WHITE_MAX = 69;
const int RED_SIZE = 1;
const int RED_MAX = 26;

int pickOne(vector<int>& balls)
{
    // initial seed by time
    unsigned seed = (unsigned)chrono::system_clock::now().time_since_epoch().count();
    // initial generator by seed
    minstd_rand0 generator(seed);
    
    // choose a random position
    int chosen = generator() % balls.size();
    int picked = balls[chosen];
    balls.erase(balls.begin() + chosen);
    
    return picked;
}

vector<int> pickBalls(int total, int count)
{
    // prepare balls
    vector<int> balls;
    for (int i = 1; i <= total; ++i)
    {
        balls.push_back(i);
    }
    
    // pick balls
    vector<int> results;
    for (int i = 1; i <= count; ++i)
    {
        results.push_back(pickOne(balls));
    }
    
    return results;
}

vector<int> pickWhites()
{
    return pickBalls(WHITE_MAX, WHITE_SIZE);
}

vector<int> pickReds()
{
    return pickBalls(RED_MAX, RED_SIZE);
}

void quickPick()
{
    for (auto i: pickWhites())
    {
        cout << i << " ";
    }
    cout << "+ ";
    for (auto i: pickReds())
    {
        cout << i << " ";
    }
    cout << endl;
}

void analyze()
{
    const int TOTAL = 100000;
    map<int, int> count;
    
    for (int i = 0; i < TOTAL; ++i)
    {
        auto r = pickWhites();
        for (auto n: r)
        {
            ++count[n];
        }
    }
    
    for (auto& p: count)
    {
        cout << setw(2) << p.first << ": " << string(p.second * 200 / TOTAL, '*') << endl;
    }
}

void rollTheDrum(Drum* drum, bool* rolling)
{
    while (*rolling)
    {
        drum->roll();
        
        this_thread::sleep_for(chrono::milliseconds(100));
    }
}

void peekTheDrum(Drum* drum)
{
    while (true)
    {
        for (auto& b: drum->peek())
        {
            cout << b.text() << " ";
        }
        cout << endl;
        
        this_thread::sleep_for(chrono::seconds(1));
    }
}

void pickBallsFromTheDrum(Drum* drum, int count)
{
    for (int i = 0; i < count; ++i)
    {
        this_thread::sleep_for(chrono::seconds(2));
        
        Ball b = drum->draw();
        cout << b.text() << " ";
        cout.flush();
    }
}

int main()
{
    // prepare drums
    Drum whiteDrum, redDrum;
    
    // put balls in
    for (int i = 1; i <= WHITE_MAX; ++i)
    {
        whiteDrum.fill(Ball(i, Ball::Color::White));
    }
    for (int i = 1; i <= RED_MAX; ++i)
    {
        redDrum.fill(Ball(i, Ball::Color::Red));
    }
    
    // start rolling
    bool rolling = true;
    thread rollWhiteThread(rollTheDrum, &whiteDrum, &rolling);
    thread rollRedThread(rollTheDrum, &redDrum, &rolling);
    
//    // peeking the drum
//    thread peekThread(peekTheDrum, whiteDrum);
    
    // start drawing
    thread drawWhiteThread(pickBallsFromTheDrum, &whiteDrum, WHITE_SIZE);
    drawWhiteThread.join();
    thread drawRedThread(pickBallsFromTheDrum, &redDrum, RED_SIZE);
    drawRedThread.join();
    cout << endl;
    
    // end
    rolling = false;
    rollWhiteThread.join();
    rollRedThread.join();
    
    return 0;
}
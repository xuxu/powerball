//
//  Ball.hpp
//  PowerBall
//
//  Created by Vivo Xu on 1/14/16.
//  Copyright © 2016 hiVivo. All rights reserved.
//

#ifndef Ball_hpp
#define Ball_hpp

#include <string>

class Ball
{
public:
    enum class Color
    {
        White,
        Red
    };
    
    Ball(int number, Color color);
    
    std::string text();
    
private:
    int m_number;
    Color m_color;
};

#endif /* Ball_hpp */
